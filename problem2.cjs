const path = require('path');
const fs = require('fs');

function problem2(fileName){

    readFile(fileName)
    .then((data) => {
        let upperCaseFileData = data.toUpperCase();
        return writeFile(upperCaseFileData,"upperCaseFile.txt");
    })
    .then((fileName) => {
        return writeFile(fileName,"filenames.txt");
    })
    .then(() => {
        return readFile("upperCaseFile.txt");
    })
    .then((data) => {
        let lowerCaseData = data.toLowerCase();
        return writeFile(lowerCaseData,"upperCaseFile.txt");
    })
    .then((fileName) => {
        return readFile(fileName);
    })
    .then((data) => {

        let paragraphArray = data.toString().split("\n\n");
        let paragraphs = paragraphArray.join();
        let sentencesArray = paragraphs.split(". ");
        let sentences = sentencesArray.join(".\n");

        return writeFile(sentences,"sentences.txt"); 
    })
    .then((fileName) => {
        return appendFileName(fileName);
    })
    .then((fileName) => {
        return readFile(fileName);
    })
    .then((data) => {

        let sentencesArray = data.split("\n");
        let sortedSentencesArray = sentencesArray.sort()
        .slice(1);
        let sortedSentences = sortedSentencesArray.join("\n");

        return writeFile(sortedSentences,"sortedSentences.txt");
    })
    .then((fileName) => {
        return appendFileName(fileName);
    })  
    .then(() => {
        return readFile("filenames.txt");
    })
    .then((data) => {
        let fileNamesArray = data.split("\n");
        return deleteFiles(fileNamesArray);
    })
    .then(() => {
        console.log("All files deleted");
    })
    .catch((err) => {
        console.error(err);
    });
}

function readFile(fileName){

    return new Promise((resolve,reject) => {

        fs.readFile(path.join(__dirname,fileName),"utf-8",function(err,data) {

            if(err){
                console.log(`${fileName} not read`);
                reject(err);

            } else {
                console.log(`${fileName} read`);
                resolve(data);

            }
        });
    });
}

function writeFile(fileData,fileName){

    return new Promise((resolve,reject) => {
        
        fs.writeFile(path.join(__dirname,fileName),fileData,function(err){

            if(err){
                console.log(`${fileName} not written`);
                reject(err);

            } else {
                console.log(`${fileName} written`);
                resolve(fileName);

            }
        });
        
    });
}

function appendFileName(fileName){

    return new Promise((resolve,reject) => {

        fs.appendFile(path.join(__dirname,"filenames.txt"),`\n${fileName}`,function(err) {

            if(err){
                console.log("filenames.txt not appended");
                reject(err);

            } else {
                console.log("filenames.txt appended");
                resolve(fileName);

            }
        });
    });
}

function deleteFiles(fileNamesArray){
    
    return Promise.all(fileNamesArray.map((fileName) => {

        return new Promise((resolve,reject) => {

            fs.unlink(path.join(__dirname,`${fileName}`),function(err){

                if(err) {
                    console.log(`${fileName} not deleted`);
                    reject(err);

                } else {
                    console.log(`${fileName} deleted`);
                    resolve(fileName);

                }
            });
        });
        
    }));
    
}

module.exports = problem2;

