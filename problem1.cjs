const path = require('path');
const fs = require('fs');

function problem1(maximumFileCount = 10, directoryName = "randomFiles"){

    createDirectory(directoryName)
    .then((directoryName) => {
        return addFiles(maximumFileCount,directoryName);
    })
    .then((fileNamesArray) => {
        console.log("All files created");
        return deleteFiles(fileNamesArray,directoryName);
    })
    .then(() => {
        console.log("All files deleted");
    })
    .catch((err) => {
        console.error(err);
    });
}

function createDirectory(directoryName){
    
    return new Promise((resolve,reject) => {

        fs.mkdir(path.join(__dirname,directoryName),{recursive: true},function(err){

            if(err){
                console.log(`${directoryName} not created`);
                reject(err);

            } else {
                console.log(`${directoryName} created`);
                resolve(directoryName);

            }
        });
    });
}


function addFiles(maximumFileCount,directoryName){

    let fileCount = Math.floor(Math.random()*maximumFileCount) + 1;
    let fileCountArray = Array.from(new Array(fileCount).keys());

    let fileNamesArray = fileCountArray.map((fileCount) => {
        return `file${fileCount+1}.json`;
    });

    return Promise.all(fileNamesArray.map((fileName) => {

        return new Promise((resolve,reject) => {

            fs.writeFile(path.join(__dirname,`${directoryName}/${fileName}`),JSON.stringify(`Created ${fileName}`),function(err){

                if(err){
                    console.log(`${fileName} not created`);
                    reject(err);

                } else {
                    console.log(`${fileName} created`);
                    resolve(fileName);

                }
            });
        });
    }));
}

function deleteFiles(fileNamesArray,directoryName){

    return Promise.all(fileNamesArray.map((fileName) => {

        return new Promise((resolve,reject) => {

            fs.unlink(path.join(__dirname,directoryName,fileName),function(err) {

                if(err){
                    console.log(`${fileName} not deleted`);
                    reject(err);

                } else {
                    console.log(`${fileName} deleted`);
                    resolve(fileName);

                }
            });
        });
    }));
}

module.exports = problem1;


